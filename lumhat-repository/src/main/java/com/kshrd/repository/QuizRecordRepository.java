package com.kshrd.repository;

import com.kshrd.model.QuizRecord;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.springframework.stereotype.Repository;

@Repository
public interface QuizRecordRepository {

    @Insert("INSERT INTO lh_quiz_record (record_date, score, duration, user_id, quiz_id) VALUES " +
            "(now(), #{score}, #{duration}, #{userId}, #{quiz.id})")
    void insert(QuizRecord quizRecord);


    @Delete("DELETE FROM lh_quiz_record r where r.user_id = #{userId}")
    void deleteAllRecord(Integer userId);
}
