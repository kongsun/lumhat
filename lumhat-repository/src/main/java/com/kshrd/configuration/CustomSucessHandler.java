package com.kshrd.configuration;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
@Component
public class CustomSucessHandler implements AuthenticationSuccessHandler {
    @Override
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
                                        Authentication authentication) throws IOException, ServletException {
        HttpSession session=httpServletRequest.getSession();
        session.setMaxInactiveInterval(100000*500000000);
        String redirectUrl=( String)httpServletRequest.getSession().getAttribute("Success");
        if(redirectUrl==null) {
            for (GrantedAuthority auth : authentication.getAuthorities()) {
                if (auth.getAuthority().equals("ADMIN")) {
                    redirectUrl = "/admin";
                } else
                    redirectUrl = "/home";
            }
        }
        httpServletRequest.getSession().setAttribute("cURL", httpServletRequest.getRequestURI());
            httpServletResponse.sendRedirect(redirectUrl);
        }
}
