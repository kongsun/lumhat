$(document).ready(function () {
    $("#btn-delete").click(function () {
        $.ajax({
            url: '/profile/delete',
            type: 'GET',
            success: function(result) {
                // Do something with the result
                removeHistoryFromUI()
            }
        })
    })
});


function removeHistoryFromUI() {
    $("#history-blog").empty();
    $("#history-blog").append('<h2 th:if="${histories.isEmpty()}" id="no-record-label">No History Record!</h2>\n')
}