function editRole(element) {
    userId = $(element).parent().parent().children().eq(0).text();
    firstName = $(element).parent().parent().children().eq(1).text();
    lastName = $(element).parent().parent().children().eq(2).text();
    role = $(element).parent().parent().children().eq(3).text();
    $("select option").each(function () {
        if ($(this).text() == role) {
            console.log(role);
            $(".txtId").val(userId);
            $(this).attr("selected", "selected");
            $("#exampleModalLabel").text('Choose new role for user "'+firstName+' '+lastName+'"')
        } else {
            $(this).removeAttr("selected", "selected")
        }
    });
}

$(function () {
    $("#btnUpdate").click(function (event) {
        event.preventDefault();
        var data={
            id:0,
            userId:$("#txtId").val(),
            roleId:$("#cboRole").val()
        };
        $.ajax({
            url:"/admin/updateRole",
            type:"POST",
            data:JSON.stringify(data),
            contentType: "application/json",
            beforeSend:function(){
                $("#basicExampleModal").modal("hide");
                $('#loading').show();
            },
            success:function (data) {
                getData("/admin/findAllUser","#tblUser")
                $("#basicExampleModal").modal("hide");
                $('#loading').hide();
            },
            error:function (data) {
                console.log("Error=> "+data);
            }
        });

    });
})

function getData(dataUrl, table) {
    $('#loading').show();
    $.ajax({
        url: dataUrl,
        type: "GET",
        beforeSend: function(){
            $('#loading').show();
        },
        success: function (data) {
            $(table).html(data);
            $('#loading').hide();
        },
        error: function (data) {
            console.log("Error : " + data);
        }
    });
}
