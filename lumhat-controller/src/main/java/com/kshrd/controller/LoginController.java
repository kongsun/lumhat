package com.kshrd.controller;

import com.kshrd.model.User;
import com.kshrd.service.UserService;
import com.kshrd.service.history.HistoryService;
import com.kshrd.service.quizRecord.QuizRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;


@Controller
public class LoginController {
    private UserService userService ;
    private HistoryService historyService;
    private QuizRecordService quizRecordService;

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setHistoryService(HistoryService historyService) {
        this.historyService = historyService;
    }

    @Autowired
    public void setQuizRecordService(QuizRecordService quizRecordService) {
        this.quizRecordService = quizRecordService;
    }
    @GetMapping("/login")
    public String loginpage(){
        return "login";
    }
    @GetMapping(value = {"/indexLogin"})
    public String home()
    {
        return "indexLogin";
    }
    @GetMapping(value = {"/profile"})
    public String profileLog(ModelMap map){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User) auth.getPrincipal();
        map.addAttribute("user",userService.findOne(user.getFacebookId())) ;
        Authentication auth1 = new UsernamePasswordAuthenticationToken(userService.findOne(user.getFacebookId()),null, user.getRoles());
        SecurityContextHolder.getContext().setAuthentication(auth1);
//        Add History not yet complete
        map.addAttribute("histories",historyService.findHistoryByUserId(user.getId()));
        return "profile";
    }

    @PostMapping("/profile")
    public String add(@ModelAttribute("user") User user) {
        userService.update(user);
        return "redirect:/profile";
    }

    @GetMapping("/profile/delete")
    public String deleteAllHistory(){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User) auth.getPrincipal();
        if (user.getId() != 0){
            quizRecordService.deleteAllRecord(user.getId());
        }
        return "redirect:/profile";
    }
}
